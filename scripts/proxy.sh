#!/usr/bin/bash
cd ~/masterthesis/src/

proxyport=$1 #port to listen to

realhost=$2 #host to send messages to
realport=$3 #host port

echo "[proxy localhost:$proxyport -> $realhost:$realport]"
# make sure a pipe called pipe is in the working directory.
# Use the command "mkfifo pipe" to create one if needed


traptrap() {
    echo "[Connection Closed]"

}

#if something goes wrong, still process the data
trap traptrap 0


nc -l -p $proxyport < pipe                   | #receive input
    ts "%.s:_INPUT:"                         | #tag the messages FROM
    tee incoming.log                         | #log the incoming messages
    sed -u 's/^[0-9]\+\.[0-9]\+:_INPUT: //g' | #remove tagging information
    nc $realhost $realport                   | #send messages trough
    ts "%.s:OUTPUT:"                         | #tag the messages TO the client
    tee outgoing.log                         | #log the outgoing messages
    sed -u 's/^[0-9]\+\.[0-9]\+:OUTPUT: //g' | #remove tagging information
    cat > pipe                                 #loop back to pipe

#     sed -u "s/^[0-9]\+i//g" |
#     sed -u "s/^[0-9]\+o//g" |

echo "[Processing information]"
#combine incoming and outgoing logs
#save only relative order of messages.
cat incoming.log outgoing.log | sort | sed 's/^[0-9]\+\.[0-9]\+://g' > fulllog.log
cat fulllog.log | python3 build.py > protocol.ditaa
rm protocol.png
ditaa protocol.ditaa protocol.png
