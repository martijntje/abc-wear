#!/usr/bin/env python3

import random
import sys

# uses /dev/urandom
rng = random.SystemRandom()

#Length in bits for N, where P*Q=N
# 100 is nice round number
# would like to have this higher but there
# seems to be a bottleneck around 108 bits

product_bit_length=100
#Length in bits for P,Q
prime_bit_length=product_bit_length//2

#set thes variables to "None" to compute new values
Public_N  = 734135512403641199605378052893
Public_e  = 33240239
Private_d = 110654164766784659917279907279
# Public_N  = None
# Public_e  = None
# Private_d = None

#just passt the value to sign as an argument
number_to_sign = sys.argv[1]

def is_prime(n):
    # initial divider to test
    p=2

    #no need to test numbers larger than the root of n
    while p*p<n:
        if (n%p) == 0:
            #if any number divides n cleanly, return false
            return False
        else:
            p+=1
    # If no divider can be found, n is prime
    return True

def is_prime_fast(n):
    # check simple cases
    if n<=1:
        return False
    elif n<=3:
        return True
    elif (n%2)==0 or (n%3)==0:
        return False
    else:
        pass

    # fermat test
    a=2
    if pow(a,n-1,n) != 1:
        return False

    # Check other dividers with larger steps
    i = 5
    while (i*i) <= n:
        if ((n%i)==0) or ((n%(i+2))==0):
            return False
        i = i + 6
    return True

# probabilistic, may not be correct
def is_prime_fermat(x):
    # 100 trials should be enough
    first_100_primes=[2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,
                      61,67,71,73,79,83,89,97,101,103,107,109,113,
                      127,131,137,139,149,151,157,163,167,173,179,
                      181,191,193,197,199,211,223,227,229,233,239,
                      241,251,257,263,269,271,277,281,283,293,307,
                      311,313,317,331,337,347,349,353,359,367,373,
                      379,383,389,397,401,409,419,421,431,433,439,
                      443,449,457,461,463,467,479,487,491,499,503,
                      509,521,523,541]

    for a in first_100_primes:
        if pow(a,x-1,x) != 1:
            return False

    # Useful check for debuggin but is a bottleneck
    #assert(is_prime_fast(x) == True)

    return True

def gen_prime(bit_length):
    # set number range
    min_size = 2 ** (bit_length-1)
    max_size = 2 ** (bit_length)
    number_range = max_size - min_size

    #pick a random number in the range
    random_number = int(min_size + \
                        number_range * rng.random())

    # if our chosen number is not prime,
    # pick a new one until we pick a prime
    while not is_prime_fermat(random_number):
        random_number = int(min_size + \
                            number_range * rng.random())

    # once we are sure we have a prime number, return it
    return random_number

def gen_special_prime(bit_length):
    # Generate a P' and P
    # We assume P' is already prime
    p_small = gen_prime(bit_length-1)
    p_large = p_small * 2 + 1

    # if P is not prime, pick new P' and P
    while not is_prime_fermat(p_large):
        p_small = gen_prime(bit_length-1)
        p_large = p_small * 2 + 1

    # once P' and P are prime, return P
    return p_large

#slow version
def invmod(x,n):
    for z in range(n):
        if (z * x)%n == 1:
            return z

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def invmod_fast(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m

#dont recompute if you have set values manually
if Public_N==None or\
   Public_e==None or\
   Private_d==None:

    print("Generating P")
    P = gen_prime(prime_bit_length)
    print("Generating Q")
    Q = gen_prime(prime_bit_length)
    N = P * Q

    # pick a small e for quick signature checking
    e = gen_prime(prime_bit_length/2)
    d = None

    # sometimes we pick a bad e
    while d==None:
        try:
            print("Generating d")
            d = invmod_fast(e , (P-1)*(Q-1))
        except:
            e = gen_prime(prime_bit_length/2)
            d = None


    #publicise this new knowledge
    print("Public_N  = %s"%N)
    print("Public_e  = %s"%e)
    print("Private_d = %s"%d)
else:
    N = Public_N
    e = Public_e
    d = Private_d

# Check that e,d are a matching pair.
for m in [2,17,35,63]: #random messages
    assert(pow(pow(m,e,N),d,N))

M = int(number_to_sign)
S = pow(M,d,N)

#final check
assert(M == pow(S,e,N))

print("N = %s"%N)
print("e = %s"%e)
print("S = %s"%S)
