#!/usr/bin/env python3

import base64

s0='%s'%"Hello world"
print(s0)
s1=s0.encode('utf-8')
print(s1)
s2=base64.b64encode(s1)
print(s2)
s3=base64.b64decode(s2)
print(s3)
s4=s3.decode('utf-8')
print(s4)
