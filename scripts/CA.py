#!/usr/bin/env python3

import random

# uses /dev/urandom
rng = random.SystemRandom()

#Length in bits for N, where P*Q=N
product_bit_length=16
#Length in bits for P,Q
prime_bit_length=product_bit_length//2

# Arbitrary for now
number_of_attributes = 16;

# Public information is either hardcoded or confirmed using a
# signature

# in this case we hardcode the needed modulus and exponent to verify this
public_knowledge_N = 734135512403641199605378052893
public_knowledge_e = 33240239

# If False, recompute P,Q,N,d,e,QR_n
precomp_done = True

#pregeneated variables
Public_CA_variables={'e': 223, 'QR_n': [31047, 19352, 13792, 20319, 10264, 21643, 27152, 35211, 1154, 1601, 16873, 6348, 14311, 4291, 22554, 1462, 37576], 'N': 37909}
Private_CA_variables={'P': 167, 'd': 5047, 'Q': 227}

def is_prime(n):
    # initial divider to test
    p=2

    #no need to test numbers larger than the root of n
    while p*p<n:
        if (n%p) == 0:
            #if any number divides n cleanly, return false
            return False
        else:
            p+=1
    # If no divider can be found, n is prime
    return True

def gen_prime(bit_length):
    # set number range
    min_size = 2 ** (bit_length-1)
    max_size = 2 ** (bit_length)
    number_range = max_size - min_size

    #pick a random number in the range
    random_number = int(min_size + \
                        number_range * rng.random())

    # if our chosen number is not prime,
    # pick a new one until we pick a prime
    while not is_prime(random_number):
        random_number = int(min_size + \
                            number_range * rng.random())

    # once we are sure we have a prime number, return it
    return random_number

def gen_special_prime(bit_length):
    # Generate a P' and P
    # We assume P' is already prime
    p_small = gen_prime(bit_length-1)
    p_large = p_small * 2 + 1

    # if P is not prime, pick new P' and P
    while not is_prime(p_large):
        p_small = gen_prime(bit_length-1)
        p_large = p_small * 2 + 1

    # once P' and P are prime, return P
    return p_large

def euler_criterion(a,p):
    if a==0: #edge case
        return True
    r=pow(a, (p-1)//2 , p)
    assert( r==1 or r==-1) # sanity check
    if r == 1:
        return True # there exists such an x
    if r == p-1:
        return False # There does not

def gen_QR_n(n,l):
    #start with empty set
    RQ = [];
    # fill it until it has size l
    while len(RQ) < l:
        x = rng.randrange(n)
        r = pow(x,2,n)          # r=x^2 mod 2
        #assert(euler_criterion(r,n)) # addition check, may be overkill
        if not (r in RQ): #avoid duplicates
            RQ.append(r)

    return RQ

def invmod(x,n):
    for z in range(n):
        if (z * x)%n == 1:
            return z

def is_valid_identity(identity,attributes):
    return True

def generate_proof(attributes):
    rndHex = hex(int(random.random()*1000000000))
    return "<<<" + attributes + "," + rndHex + ">>>"

if precomp_done:
    N = Public_CA_variables["N"]
    P = Private_CA_variables["P"]
    Q = Private_CA_variables["Q"]
else:
    P = gen_special_prime(prime_bit_length)
    Q = gen_special_prime(prime_bit_length)
    N = P * Q

    Public_CA_variables["N"] = N
    Private_CA_variables["P"] = P
    Private_CA_variables["Q"] = Q

if precomp_done:
        e = Public_CA_variables["e"]
        d = Private_CA_variables["d"]
else:
        e = gen_prime(prime_bit_length)
        d = invmod(e , (P-1)*(Q-1))

        Public_CA_variables["e"] = e
        Private_CA_variables["d"] = d

if precomp_done:
    QR_N = Public_CA_variables["QR_n"]
else:
    QR_N = gen_QR_n(N,number_of_attributes+1)
    Public_CA_variables["QR_n"] = QR_N

#public_parameters = N,e,QR_N

if not precomp_done:
    print("Public_CA_variables=%s"%repr(Public_CA_variables))
    print("Private_CA_variables=%s"%repr(Private_CA_variables))

use_case=input()

if use_case == "INFO":
    for key in Public_CA_variables:
        print(key, Public_CA_variables[key])
    quit()
else:
    pass #other use case is giving proof of attributes

attributes_wanted=input()

print("Prove you identity for (%s)"%attributes_wanted)
identity=input()
assert(is_valid_identity(attributes_wanted,identity))

print("Waiting for user to generate alpha")
confirmation  = input()

if is_valid_identity(attributes_wanted,identity):
    proof_of_attributes = generate_proof(attributes_wanted)
    print(proof_of_attributes)
else:
    print("ERROR: invalid identity")
