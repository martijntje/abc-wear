#!/usr/bin/env python3
import sys

lines=sys.stdin.readlines()
lines=list(map(lambda x:x[:-1], lines ))


pre_inp="_INPUT: "
pre_out="OUTPUT: "
realstr=lambda x:x[len(pre_inp):]
prefixstr=lambda x:x[:len(pre_inp)]
boxes=[
    ("+-----+","+-----+"),
    ("|cBLU |","|cRED |"),
    ("|  A  |","|  B  |"),
    ("|     |","|     |"),
    ("+--+--+","+--+--+")]


#add padding to string s, to make its length l
#make it centered
def pad_str(s,l):
    if len(s)>=l:
        return s
    elif len(s)+1==l:
        return s+" "
    else:
        return pad_str(" "+s+" ",l)


#loop one to find the length of the longest message
#length of longest message send
max_mess_len=4
for line in lines:
    if len(realstr(line))>max_mess_len:
        max_mess_len=len(realstr(line))

#print boxes
for l,r in boxes:
    print(l+" "*(max_mess_len-4)+r)

#print ditaa code for each message send
for line in lines:
    print("   | " + " "*max_mess_len + " |   ")
    print("   | " + pad_str(realstr(line),max_mess_len) + " |   ")
    if prefixstr(line) == pre_inp:
        print("   +-" + "-"*max_mess_len+">|   ")
    elif prefixstr(line) == pre_out:
        print("   |<" + "-"*max_mess_len+"-+   ")
    else:
        print("MALFORMED PREFIX!: %s"%repr(line))
    print("   | " + " "*max_mess_len + " |   ")
