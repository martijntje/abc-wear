#!/bin/sh

cd "$HOME/masterthesis/src/"

rm *.ditaa
rm *.png

tmux new-session -d "./listenserver \"python3 ./CA.py\" 9090"
tmux split-window 'sleep 1;./proxy.sh 8080 localhost 9090'
tmux split-window 'sleep 2;echo "[incoming]";tail -f incoming.log'
tmux split-window 'sleep 3;echo "[outgoing]";tail -f outgoing.log'
tmux select-layout tiled
tmux -2 attach-session -d

cp protocol.png protocol.ca.png
cp protocol.ditaa protocol.ca.ditaa

tmux new-session -d "./listenserver \"python3 ./service.py\" 9090"
tmux split-window 'sleep 1;./proxy.sh 8080 localhost 9090'
tmux split-window 'sleep 2;echo "[incoming]";tail -f incoming.log'
tmux split-window 'sleep 3;echo "[outgoing]";tail -f outgoing.log'
tmux select-layout tiled
tmux -2 attach-session -d

cp protocol.png protocol.service.png
cp protocol.ditaa protocol.service.ditaa

# feh -F protocol.ca.png
# feh -F protocol.service.png

cat protocol.ca.ditaa
cat protocol.service.ditaa
